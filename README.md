HTML text 

![Alt text][gravizo]
[gravizo]: https://g.gravizo.com/svg?digraph&space;G{size="4,4";main[shape=box];main-&rt;parse[weight=8];parse-&rt;execute;main-&rt;init[style=dotted];main-&rt;cleanup;execute-&rt;{make_string;printf}init-&rt;make_string;edge[color=red];main-&rt;printf[style=bold,label="100&space;times"];make_string[label="string"];node[shape=box,style=filled,color=".7.31.0"];execute-&rt;compare;}

[![Bitbucket issues](https://img.shields.io/bitbucket/pipelines/delthery/wiki.test1.svg?style=for-the-badge)](https://bitbucket.org/planarllc/planar.vna.portable/)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/issues/planarllc/wiki.test1.svg?style=for-the-badge)


![Alt text](https://g.gravizo.com/source/svg/thiisthemark?https%3A%2F%2Fbitbucket.org%2Fdelthery%2Fwiki.test1%2Fraw%2Fe212bd94e58879843ebc713e93e9f9dcbf21994f%2FREADME.md)
![Alt text](http://www.gravizo.com/img/1x1.png)

thiisthemark        
@startuml

object Object01 [^1]

object Object02
object Object03
object Object04
object Object05
object Object06
object Object07
object Object08

Object01 <|-- Object02
Object03 *-- Object04
Object05 o-- "7" Object06
Object07 .. Object08 : some labels
@enduml
thiisthemark        

*[HTML]: abbreviation

[^1]: Reference

